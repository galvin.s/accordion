import { Component } from '@angular/core';
import { HttpClient} from "@angular/common/http";  //might be module but didnt see this in video

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  information: any [];

  automaticClose = false;

  constructor(private http: HttpClient) { 
    this.http.get('assets/information.json').subscribe( res => {
      this.information = res['items'];

//line to auto open 1st level

      this.information[0].open = true;
    })

  }

  toggleSection(index) {
//info clicked to open
    this.information[index].open = !this.information[index].open;

    if (this.automaticClose && this.information[index].open) {
      this.information
      .filter(( item, itemIndex) => itemIndex != index)
      .map(item => item.open = false);
    }
  }
  
  toggleItem(index, childIndex){
    this.information[index].children[childIndex].open = !this.information[index].child; //blocked on screen might be childopen
  }

}
