import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from "../components/product/product.component";

//must add a created component when ionic modules are in use as in the products componenets file
import { IonicModule } from "@ionic/angular";

@NgModule({
  declarations: [ProductComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [ ProductComponent]
})
export class SharedComponentsModule { }
