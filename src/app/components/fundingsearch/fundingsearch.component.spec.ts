import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FundingsearchPage } from './fundingsearch.page';

describe('FundingsearchPage', () => {
  let component: FundingsearchPage;
  let fixture: ComponentFixture<FundingsearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FundingsearchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FundingsearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
